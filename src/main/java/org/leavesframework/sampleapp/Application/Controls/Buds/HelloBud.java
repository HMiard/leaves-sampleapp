package org.leavesframework.sampleapp.Application.Controls.Buds;

import org.leavesframework.core.framework.Objects.Application.Branch;
import org.leavesframework.core.framework.Objects.Application.Bud;
import org.leavesframework.core.webserver.Http.GenericSession;
import org.leavesframework.sampleapp.Application.Leaves.Includes.HelloLeaf;
import org.leavesframework.sampleapp.Application.Leaves.Includes.NavigationLeaf;

public class HelloBud extends Bud<HelloLeaf> {


    public HelloBud(Branch parent, String name, HelloLeaf leaf) {
        super(parent, name, leaf, "?name");
    }

    @Override
    public String execute(GenericSession session) throws Exception {

        leaf.inject("navigation", findLeaf(NavigationLeaf.class));
        leaf.inject("wrapper", findLeaf(HelloLeaf.class));

        String name = this.uriParameters.get("name");
        if (name.equals("")) name = "world";

        leaf.getElementById("display_hello").html("Hello, " + name + " !");

        return leaf.html();
    }
}
