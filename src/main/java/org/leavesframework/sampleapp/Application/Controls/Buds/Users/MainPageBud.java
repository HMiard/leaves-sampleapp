/*
 * Leaves Framework
 * Copyright (C) 2015  Hugo Miard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.leavesframework.sampleapp.Application.Controls.Buds.Users;


import org.leavesframework.core.framework.Objects.Application.Branch;
import org.leavesframework.core.framework.Objects.Application.Bud;
import org.leavesframework.core.webserver.Http.GenericSession;
import org.leavesframework.core.webserver.Http.SessionStorage;
import org.leavesframework.sampleapp.Application.Controls.Acorns.UserAcorn;
import org.leavesframework.sampleapp.Application.Leaves.Users.MainPageLeaf;

public class MainPageBud extends Bud<MainPageLeaf> {

    public MainPageBud(Branch parent, String name, MainPageLeaf leaf) {
        super(parent, name, leaf);
    }

    @Override
    public String execute(GenericSession session) throws Exception {

        UserAcorn user = (UserAcorn) SessionStorage.retrieve(session, "user");
        String name = "How did you get there ?";

        if (user != null){
            name = user.getCompleteName();
            if (session.getParameters().get("disc") != null){
                user.disconnect(session);
                this.redirect("");
            }
        }

        leaf.getElementById("welcome").html(String.format("Hello, %s", name));
        leaf.getElementById("disconnect").attr("href", "/users?disc=true");
        return leaf.html();
    }
}
