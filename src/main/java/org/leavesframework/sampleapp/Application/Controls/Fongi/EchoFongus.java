package org.leavesframework.sampleapp.Application.Controls.Fongi;

import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Utils.Commons.Objects.GenericFongus;
import org.leavesframework.core.webserver.Leaves;
import org.java_websocket.WebSocket;

import java.net.UnknownHostException;

/**
 * A websocket endpoint that send incoming messages to everyone, including the sender.
 * Is available at ws://127.0.0.1:8000/echo
 */
public class EchoFongus extends GenericFongus {

    public EchoFongus(String url, LeavesApplication app) throws UnknownHostException {
        super(url, app);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        this.say("New message from {" + hostAddress(conn) + "} : "+ Leaves.NLSP + message);
        this.sendToEveryone(message);
    }
}
