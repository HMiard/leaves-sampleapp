package org.leavesframework.sampleapp.Application.Controls.Buds.Users;


import org.leavesframework.core.framework.Objects.Application.Branch;
import org.leavesframework.core.framework.Objects.Application.Bud;
import org.leavesframework.core.webserver.Http.GenericSession;
import org.leavesframework.sampleapp.Application.Controls.Acorns.UserAcorn;
import org.leavesframework.sampleapp.Application.Leaves.Includes.ConfirmLeaf;
import org.leavesframework.sampleapp.Application.Leaves.Users.SignupLeaf;

public class SignupBud extends Bud<SignupLeaf> {

    public SignupBud(Branch parent, String name, SignupLeaf leaf) {
        super(parent, name, leaf);
    }

    @Override
    public String execute(GenericSession session) throws Exception {

        leaf.inject("wrapper", findLeaf(ConfirmLeaf.class));

        UserAcorn newUser = new UserAcorn();
        newUser.firstName = session.getParameters().get("fname");
        newUser.lastName = session.getParameters().get("lname");
        newUser.email = session.getParameters().get("email");
        newUser.password = session.getParameters().get("password");

        if (newUser.isInvalid())
            return error("Please fill all the fields accordingly !");

        if (!session.getParameters().get("rpassword").equals(newUser.password))
            return error("Passwords do not match !");

        orm.beginTransaction();
        orm.save(getSecurityContext().userHelper.hashUser(newUser));
        orm.getTransaction().commit();

        return confirmSignup(newUser);
    }


    public String confirmSignup(UserAcorn user) throws Exception {

        leaf.confirmSignup(user);
        return leaf.html();
    }

    public String error(String message) throws Exception {

        leaf.displayError(message);
        return leaf.html();
    }
}
