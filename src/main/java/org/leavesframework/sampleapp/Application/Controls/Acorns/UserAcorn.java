package org.leavesframework.sampleapp.Application.Controls.Acorns;

import org.leavesframework.core.framework.Modules.Data.Entities.BasicUserAcorn;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class UserAcorn extends BasicUserAcorn implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;

    public String getCompleteName(){
        return firstName + " " + lastName;
    }

    public boolean isInvalid(){
        return
                firstName.isEmpty() ||
                lastName.isEmpty() ||
                email.isEmpty() ||
                password.isEmpty();
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public void setUsername(String username) {
        this.email = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean useFieldExposition() {
        return true;
    }

    @Override
    public String[] exposedFields() {
        return new String[]{
                "id", "firstName", "lastName", "email"
        };
    }
}
