package org.leavesframework.sampleapp.Application.Controls.Branches;

import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.Branch;
import org.leavesframework.core.framework.Exceptions.LeafNotFoundException;
import org.leavesframework.sampleapp.Application.Controls.Buds.HelloBud;
import org.leavesframework.sampleapp.Application.Controls.Buds.IndexBud;
import org.leavesframework.sampleapp.Application.Leaves.Includes.HelloLeaf;
import org.leavesframework.sampleapp.Application.Leaves.IndexLeaf;


public class IndexBranch extends Branch {

    public IndexBranch(String name,  LeavesApplication app) {

        super(name, app);

        try {

            new IndexBud(this, "", findLeaf(IndexLeaf.class));
            new HelloBud(this, "hello", findLeaf(HelloLeaf.class));

        } catch (LeafNotFoundException e) {
            e.printStackTrace();
        }
    }


}
