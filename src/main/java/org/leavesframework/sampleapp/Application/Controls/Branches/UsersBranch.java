package org.leavesframework.sampleapp.Application.Controls.Branches;

import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.Branch;
import org.leavesframework.core.framework.Exceptions.LeafNotFoundException;
import org.leavesframework.core.framework.Utils.Commons.Security.Fallbacks.RedirectionFallback;
import org.leavesframework.core.framework.Utils.Commons.Security.Assertions.UserConnectedAssertion;
import org.leavesframework.sampleapp.Application.Controls.Buds.Users.LoginBud;
import org.leavesframework.sampleapp.Application.Controls.Buds.Users.MainPageBud;
import org.leavesframework.sampleapp.Application.Controls.Buds.Users.SignupBud;
import org.leavesframework.sampleapp.Application.Controls.Acorns.UserAcorn;
import org.leavesframework.sampleapp.Application.Leaves.Users.SignupLeaf;
import org.leavesframework.sampleapp.Application.Leaves.Users.LoginLeaf;
import org.leavesframework.sampleapp.Application.Leaves.Users.MainPageLeaf;

public class UsersBranch extends Branch {

    public UsersBranch(String name,  LeavesApplication app) {

        super(name, app);

        try {

            new SignupBud(this, "signup", findLeaf(SignupLeaf.class));
            new LoginBud(this, "login", findLeaf(LoginLeaf.class));
            new MainPageBud(this, "", findLeaf(MainPageLeaf.class));

            this.getFirewall().protectBud("", new UserConnectedAssertion(
                    new RedirectionFallback("users/login"), UserAcorn.class
            ));
            this.getFirewall().protectBud("login", new UserConnectedAssertion(
                    new RedirectionFallback("users"), UserAcorn.class
            ).reverse());

        } catch (LeafNotFoundException e) {
            e.printStackTrace();
        }
    }

}
