package org.leavesframework.sampleapp.Application.Controls.Buds.Users;

import org.hibernate.criterion.Restrictions;
import org.jsoup.nodes.Element;
import org.leavesframework.core.framework.Objects.Application.Branch;
import org.leavesframework.core.framework.Objects.Application.Bud;
import org.leavesframework.core.framework.Utils.Html.HtmlGenerator;
import org.leavesframework.core.webserver.Http.Exceptions.NonExistentSessionException;
import org.leavesframework.core.webserver.Http.GenericSession;
import org.leavesframework.sampleapp.Application.Controls.Acorns.UserAcorn;
import org.leavesframework.sampleapp.Application.Leaves.Users.LoginLeaf;

import java.util.List;

public class LoginBud extends Bud<LoginLeaf> {

    public LoginBud(Branch parent, String name, LoginLeaf leaf) {
        super(parent, name, leaf);
    }

    @Override
    public String execute(GenericSession session) throws Exception {

        String email = session.getParameters().get("email");
        String password = session.getParameters().get("password");

        if (email != null && password != null){

            if (email.equals("") || password.equals(""))
                leaf.getElementById("error_display").html(
                        "Please fill all the fields accordingly !"
                );

            else {
                if (!confirmAndLogIn(email, password, session))
                    leaf.getElementById("error_display").html(
                            "Wrong credentials !"
                    );
                else
                    redirect("users");
            }
        }
        generateLoginForm(leaf.getElementById("login_form"));
        return leaf.html();
    }

    /**
     * Trying to find the user with these credentials.
     * If the user is found, we open his session.
     *
     * @param email String
     * @param password String
     * @return boolean
     */
    public boolean confirmAndLogIn(String email, String password, GenericSession session)
            throws NonExistentSessionException {

        List users = orm.createCriteria(UserAcorn.class)
                .add(Restrictions.eq("email", email))
                .add(Restrictions.eq("password", getSecurityContext().getHashProvider().hash(password)))
                .list();

        if (users.isEmpty()) return false;

        UserAcorn user = (UserAcorn) users.get(0);
        user.connect(session);
        return true;
    }


    public void generateLoginForm(Element parent){

        if (parent == null) return;
        parent.appendChild(HtmlGenerator.generateLoginForm("login", true));
    }
}
