package org.leavesframework.sampleapp.Application.Controls.Branches;

import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Modules.Rest.Application.RestfulBranch;
import org.leavesframework.sampleapp.Application.Modules.Rest.UsersSyrupJar;

public class ApiBranch extends RestfulBranch {

    public ApiBranch(String name, LeavesApplication app) {

        super(name, app);

        this.attach(new UsersSyrupJar(this.getORM()));

        this.spill();
    }


}
