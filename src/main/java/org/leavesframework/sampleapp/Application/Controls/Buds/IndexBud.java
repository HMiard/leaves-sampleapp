package org.leavesframework.sampleapp.Application.Controls.Buds;

import org.leavesframework.core.framework.Objects.Application.Branch;
import org.leavesframework.core.framework.Objects.Application.Bud;
import org.leavesframework.core.webserver.Http.GenericSession;
import org.leavesframework.sampleapp.Application.Leaves.Users.SignupLeaf;
import org.leavesframework.sampleapp.Application.Leaves.Includes.UsersLeaf;
import org.leavesframework.sampleapp.Application.Leaves.IndexLeaf;

public class IndexBud extends Bud<IndexLeaf> {

    public IndexBud(Branch parent, String name, IndexLeaf leaf) {
        super(parent, name, leaf);
    }

    @Override
    public String execute(GenericSession session) throws Exception {

        leaf.inject("wrapper", findLeaf(UsersLeaf.class));
        leaf.inject("wrapper", findLeaf(SignupLeaf.class));

        leaf.appendUsers(leaf.getElementById("users_display"));
        leaf.generateRegistrationForm(leaf.getElementById("registration_form"));

        leaf.getElementById("login_link").attr("href", "users/login");
        leaf.getElementById("users_link").attr("href", "users");

        return leaf.html();
    }
}
