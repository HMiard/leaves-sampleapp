package org.leavesframework.sampleapp.Application.Leaves.Users;

import org.jsoup.nodes.Document;;
import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.Leaf;
import org.leavesframework.sampleapp.Application.Leaves.LayoutLeaf;


public class MainPageLeaf extends Leaf {

	public MainPageLeaf(String name, Document dom, LeavesApplication app){ super(name, dom, app, LayoutLeaf.class, "wrapper"); }
}
