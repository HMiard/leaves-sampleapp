/*
 * Leaves Framework
 * Copyright (C) 2015  Hugo Miard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.leavesframework.sampleapp.Application.Leaves.Users;

import org.leavesframework.sampleapp.Application.Controls.Acorns.UserAcorn;
import org.jsoup.nodes.Document;;
import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.Leaf;
import org.leavesframework.sampleapp.Application.Leaves.LayoutLeaf;

import javax.xml.xpath.XPathExpressionException;


public class SignupLeaf extends Leaf {

	public SignupLeaf(String name, Document dom, LeavesApplication app){ super(name, dom, app, LayoutLeaf.class, "wrapper"); }


    public void confirmSignup(UserAcorn user) throws XPathExpressionException {

        this.getElementById("confirm-title").html(
                "Signup complete !"
        );
        this.getElementById("confim-message").html(
                "Thank you, " + user.getCompleteName()
        );
    }


    public void displayError(String message) throws XPathExpressionException {

        this.getElementById("confirm-title").html(
                "Whoops ! An error occurred."
        );
        this.getElementById("confim-message").html(message);
    }
}
