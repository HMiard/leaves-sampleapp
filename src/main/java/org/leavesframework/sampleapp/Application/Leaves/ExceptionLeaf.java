package org.leavesframework.sampleapp.Application.Leaves;

import org.jsoup.nodes.Document;
import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.Leaf;


public class ExceptionLeaf extends Leaf {

	public ExceptionLeaf(String name, Document dom, LeavesApplication app){ super(name, dom, app); }
}
