package org.leavesframework.sampleapp.Application.Leaves;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.leavesframework.core.framework.Utils.Html.HtmlGenerator;
import org.leavesframework.sampleapp.Application.Controls.Acorns.UserAcorn;
import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.Leaf;
import org.leavesframework.sampleapp.Application.Modules.Rest.UsersSyrupJar;

import java.util.List;

public class IndexLeaf extends Leaf {

	public UsersSyrupJar usersSyrupJar;

	public IndexLeaf(String name, Document dom, LeavesApplication app){

        super(name, dom, app, LayoutLeaf.class, "wrapper");

        this.usersSyrupJar = new UsersSyrupJar(getORM());
    }

	/**
	 * Creating a list of registered users.
	 *
	 * @param userDisplay Node
	 */
	public void appendUsers(Element userDisplay){

		if (userDisplay == null) return;

		List<UserAcorn> users = this.usersSyrupJar.findAll();

		if (users.size() == 0)
			userDisplay.html("No user registered yet.");

		for (UserAcorn user : users){

            Element userBlock = userDisplay.appendElement("div").attr("class", "user row");
            userBlock.appendElement("div").attr("class", "col-lg-6").html(user.getCompleteName());
            userBlock.appendElement("div").attr("class", "col-lg-6").html(user.email);
		}

	}

	public void generateRegistrationForm(Element parent){

		if (parent == null) return;
		Element form = HtmlGenerator.generateRegistrationForm(
				"users/signup", true
		);
		if (form != null)
			parent.appendChild(form);
	}
}
