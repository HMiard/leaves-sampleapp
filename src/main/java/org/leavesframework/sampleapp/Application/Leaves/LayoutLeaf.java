package org.leavesframework.sampleapp.Application.Leaves;

import org.jsoup.nodes.Document;
import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.Leaf;
import org.leavesframework.sampleapp.Application.Leaves.Includes.NavigationLeaf;

public class LayoutLeaf extends Leaf {

    public LayoutLeaf(String name, Document dom, LeavesApplication app) {
        super(name, dom, app);
    }

    @Override
    public void preExecute() throws Exception {

        this.inject("navigation", findLeaf(NavigationLeaf.class));
    }
}
