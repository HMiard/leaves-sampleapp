package org.leavesframework.sampleapp.Application.Leaves.Includes;

import org.jsoup.nodes.Document;;
import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.Leaf;


public class NavigationLeaf extends Leaf {

	public NavigationLeaf(String name, Document dom, LeavesApplication app){ super(name, dom, app); }
}
