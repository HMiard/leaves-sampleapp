package org.leavesframework.sampleapp.Application.Modules.Rest;

import org.hibernate.Session;
import org.leavesframework.core.framework.Modules.Data.Annotations.OnAWaffle;
import org.leavesframework.core.framework.Utils.Commons.Modules.Data.SafeUserJar;
import org.leavesframework.sampleapp.Application.Controls.Acorns.UserAcorn;

@OnAWaffle
public class UsersSyrupJar extends SafeUserJar<UserAcorn> {

    public UsersSyrupJar(Session ORM) {
        super(ORM);
    }

    @Override
    public Class<UserAcorn> getEntityClass() {
        return UserAcorn.class;
    }
}
