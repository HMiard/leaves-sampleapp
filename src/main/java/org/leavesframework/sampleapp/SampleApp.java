package org.leavesframework.sampleapp;

import org.leavesframework.core.LeavesApplication;
import org.leavesframework.core.framework.Objects.Application.LeavesConfiguration;

public class SampleApp {

    public static void main(String[] args){

        new LeavesApplication(args, new LeavesConfiguration(SampleApp.class), true);
    }
}
